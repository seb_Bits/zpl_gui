﻿namespace ZPL_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Connect_RZ400 = new System.Windows.Forms.Button();
            this.btn_write = new System.Windows.Forms.Button();
            this.cmbx_WrBank = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Tries = new System.Windows.Forms.Label();
            this.txtxbx_WrMyTries = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbx_WrChunkSize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbx_WrResult = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbx_WrData = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbx_WrAddr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_read = new System.Windows.Forms.Button();
            this.cmbx_Bank = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbx_Addr = new System.Windows.Forms.TextBox();
            this.Address = new System.Windows.Forms.Label();
            this.txtbx_NumWrdsRead = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbx_RdData = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_ConnectZD500R = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtbx_RdEPCSER = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtbx_RdEPCPNO = new System.Windows.Forms.TextBox();
            this.btn_Clear_RdEPC = new System.Windows.Forms.Button();
            this.btn_Read_EPC = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_Clear_WrEPC = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbx_WrEPCSER = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtbx_WrEPCPNO = new System.Windows.Forms.TextBox();
            this.btn_Write_EPC = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Connect_RZ400
            // 
            this.btn_Connect_RZ400.Location = new System.Drawing.Point(493, 30);
            this.btn_Connect_RZ400.Name = "btn_Connect_RZ400";
            this.btn_Connect_RZ400.Size = new System.Drawing.Size(141, 34);
            this.btn_Connect_RZ400.TabIndex = 1;
            this.btn_Connect_RZ400.Text = "Connect to RZ400";
            this.btn_Connect_RZ400.UseVisualStyleBackColor = true;
            this.btn_Connect_RZ400.Visible = false;
            this.btn_Connect_RZ400.Click += new System.EventHandler(this.btn_Connect_RZ400_Click);
            // 
            // btn_write
            // 
            this.btn_write.Location = new System.Drawing.Point(17, 27);
            this.btn_write.Name = "btn_write";
            this.btn_write.Size = new System.Drawing.Size(134, 50);
            this.btn_write.TabIndex = 9;
            this.btn_write.Text = "Write";
            this.btn_write.UseVisualStyleBackColor = true;
            this.btn_write.Click += new System.EventHandler(this.btn_write_Click);
            // 
            // cmbx_WrBank
            // 
            this.cmbx_WrBank.FormattingEnabled = true;
            this.cmbx_WrBank.Items.AddRange(new object[] {
            "Reserved",
            "Epc",
            "Tid",
            "User"});
            this.cmbx_WrBank.Location = new System.Drawing.Point(17, 103);
            this.cmbx_WrBank.Name = "cmbx_WrBank";
            this.cmbx_WrBank.Size = new System.Drawing.Size(72, 21);
            this.cmbx_WrBank.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.Tries);
            this.groupBox2.Controls.Add(this.txtxbx_WrMyTries);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtbx_WrChunkSize);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtbx_WrResult);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtbx_WrData);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtbx_WrAddr);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbx_WrBank);
            this.groupBox2.Controls.Add(this.btn_write);
            this.groupBox2.Location = new System.Drawing.Point(36, 280);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(312, 166);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Write";
            this.groupBox2.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(253, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 26);
            this.label8.TabIndex = 24;
            this.label8.Text = "Reader\r\nRetries";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(252, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(45, 20);
            this.textBox1.TabIndex = 23;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Tries
            // 
            this.Tries.AutoSize = true;
            this.Tries.Location = new System.Drawing.Point(251, 86);
            this.Tries.Name = "Tries";
            this.Tries.Size = new System.Drawing.Size(47, 13);
            this.Tries.TabIndex = 22;
            this.Tries.Text = "My Tries";
            // 
            // txtxbx_WrMyTries
            // 
            this.txtxbx_WrMyTries.Location = new System.Drawing.Point(252, 103);
            this.txtxbx_WrMyTries.Name = "txtxbx_WrMyTries";
            this.txtxbx_WrMyTries.Size = new System.Drawing.Size(45, 20);
            this.txtxbx_WrMyTries.TabIndex = 21;
            this.txtxbx_WrMyTries.Text = "1";
            this.txtxbx_WrMyTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(180, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Chunk Size";
            // 
            // txtbx_WrChunkSize
            // 
            this.txtbx_WrChunkSize.Location = new System.Drawing.Point(188, 103);
            this.txtbx_WrChunkSize.Name = "txtbx_WrChunkSize";
            this.txtbx_WrChunkSize.Size = new System.Drawing.Size(45, 20);
            this.txtbx_WrChunkSize.TabIndex = 19;
            this.txtbx_WrChunkSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(181, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Result";
            // 
            // txtbx_WrResult
            // 
            this.txtbx_WrResult.Location = new System.Drawing.Point(165, 44);
            this.txtbx_WrResult.Name = "txtbx_WrResult";
            this.txtbx_WrResult.Size = new System.Drawing.Size(68, 20);
            this.txtbx_WrResult.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Write Data";
            // 
            // txtbx_WrData
            // 
            this.txtbx_WrData.Location = new System.Drawing.Point(18, 142);
            this.txtbx_WrData.Multiline = true;
            this.txtbx_WrData.Name = "txtbx_WrData";
            this.txtbx_WrData.Size = new System.Drawing.Size(239, 18);
            this.txtbx_WrData.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(116, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Address";
            // 
            // txtbx_WrAddr
            // 
            this.txtbx_WrAddr.Location = new System.Drawing.Point(104, 103);
            this.txtbx_WrAddr.Name = "txtbx_WrAddr";
            this.txtbx_WrAddr.Size = new System.Drawing.Size(68, 20);
            this.txtbx_WrAddr.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Bank";
            // 
            // btn_read
            // 
            this.btn_read.Location = new System.Drawing.Point(8, 12);
            this.btn_read.Name = "btn_read";
            this.btn_read.Size = new System.Drawing.Size(134, 50);
            this.btn_read.TabIndex = 0;
            this.btn_read.Text = "Read";
            this.btn_read.UseVisualStyleBackColor = true;
            this.btn_read.Click += new System.EventHandler(this.btn_read_Click);
            // 
            // cmbx_Bank
            // 
            this.cmbx_Bank.FormattingEnabled = true;
            this.cmbx_Bank.Items.AddRange(new object[] {
            "Reserved",
            "Epc",
            "Tid",
            "User"});
            this.cmbx_Bank.Location = new System.Drawing.Point(12, 86);
            this.cmbx_Bank.Name = "cmbx_Bank";
            this.cmbx_Bank.Size = new System.Drawing.Size(72, 21);
            this.cmbx_Bank.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bank";
            // 
            // txtbx_Addr
            // 
            this.txtbx_Addr.Location = new System.Drawing.Point(98, 86);
            this.txtbx_Addr.Name = "txtbx_Addr";
            this.txtbx_Addr.Size = new System.Drawing.Size(68, 20);
            this.txtbx_Addr.TabIndex = 4;
            // 
            // Address
            // 
            this.Address.AutoSize = true;
            this.Address.Location = new System.Drawing.Point(111, 71);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(45, 13);
            this.Address.TabIndex = 5;
            this.Address.Text = "Address";
            // 
            // txtbx_NumWrdsRead
            // 
            this.txtbx_NumWrdsRead.Location = new System.Drawing.Point(183, 86);
            this.txtbx_NumWrdsRead.Name = "txtbx_NumWrdsRead";
            this.txtbx_NumWrdsRead.Size = new System.Drawing.Size(68, 20);
            this.txtbx_NumWrdsRead.TabIndex = 6;
            this.txtbx_NumWrdsRead.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "# words to Read";
            // 
            // txtbx_RdData
            // 
            this.txtbx_RdData.Location = new System.Drawing.Point(12, 113);
            this.txtbx_RdData.Multiline = true;
            this.txtbx_RdData.Name = "txtbx_RdData";
            this.txtbx_RdData.Size = new System.Drawing.Size(239, 20);
            this.txtbx_RdData.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbx_RdData);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtbx_NumWrdsRead);
            this.groupBox1.Controls.Add(this.Address);
            this.groupBox1.Controls.Add(this.txtbx_Addr);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbx_Bank);
            this.groupBox1.Controls.Add(this.btn_read);
            this.groupBox1.Location = new System.Drawing.Point(357, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(278, 166);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Read";
            this.groupBox1.Visible = false;
            // 
            // btn_ConnectZD500R
            // 
            this.btn_ConnectZD500R.Location = new System.Drawing.Point(115, 31);
            this.btn_ConnectZD500R.Name = "btn_ConnectZD500R";
            this.btn_ConnectZD500R.Size = new System.Drawing.Size(127, 32);
            this.btn_ConnectZD500R.TabIndex = 13;
            this.btn_ConnectZD500R.Text = "Connect To ZD500R";
            this.btn_ConnectZD500R.UseVisualStyleBackColor = true;
            this.btn_ConnectZD500R.Click += new System.EventHandler(this.btn_ConnectZD500R_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Location = new System.Drawing.Point(386, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 181);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "EPC";
            this.groupBox3.Visible = false;
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtbx_RdEPCSER);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.txtbx_RdEPCPNO);
            this.groupBox5.Controls.Add(this.btn_Clear_RdEPC);
            this.groupBox5.Controls.Add(this.btn_Read_EPC);
            this.groupBox5.Location = new System.Drawing.Point(23, 30);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(236, 145);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Read";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(136, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Serial Number";
            // 
            // txtbx_RdEPCSER
            // 
            this.txtbx_RdEPCSER.Location = new System.Drawing.Point(126, 43);
            this.txtbx_RdEPCSER.Name = "txtbx_RdEPCSER";
            this.txtbx_RdEPCSER.Size = new System.Drawing.Size(87, 20);
            this.txtbx_RdEPCSER.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Part Number";
            // 
            // txtbx_RdEPCPNO
            // 
            this.txtbx_RdEPCPNO.Location = new System.Drawing.Point(16, 43);
            this.txtbx_RdEPCPNO.Name = "txtbx_RdEPCPNO";
            this.txtbx_RdEPCPNO.Size = new System.Drawing.Size(87, 20);
            this.txtbx_RdEPCPNO.TabIndex = 7;
            // 
            // btn_Clear_RdEPC
            // 
            this.btn_Clear_RdEPC.Location = new System.Drawing.Point(71, 117);
            this.btn_Clear_RdEPC.Name = "btn_Clear_RdEPC";
            this.btn_Clear_RdEPC.Size = new System.Drawing.Size(88, 23);
            this.btn_Clear_RdEPC.TabIndex = 6;
            this.btn_Clear_RdEPC.Text = "Clear";
            this.btn_Clear_RdEPC.UseVisualStyleBackColor = true;
            this.btn_Clear_RdEPC.Click += new System.EventHandler(this.btn_Clear_RdEPC_Click);
            // 
            // btn_Read_EPC
            // 
            this.btn_Read_EPC.Location = new System.Drawing.Point(71, 80);
            this.btn_Read_EPC.Name = "btn_Read_EPC";
            this.btn_Read_EPC.Size = new System.Drawing.Size(88, 23);
            this.btn_Read_EPC.TabIndex = 5;
            this.btn_Read_EPC.Text = "Read EPC";
            this.btn_Read_EPC.UseVisualStyleBackColor = true;
            this.btn_Read_EPC.Click += new System.EventHandler(this.btn_Read_EPC_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_Clear_WrEPC);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtbx_WrEPCSER);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtbx_WrEPCPNO);
            this.groupBox4.Controls.Add(this.btn_Write_EPC);
            this.groupBox4.Location = new System.Drawing.Point(74, 85);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(219, 145);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Write";
            // 
            // btn_Clear_WrEPC
            // 
            this.btn_Clear_WrEPC.Location = new System.Drawing.Point(56, 117);
            this.btn_Clear_WrEPC.Name = "btn_Clear_WrEPC";
            this.btn_Clear_WrEPC.Size = new System.Drawing.Size(103, 23);
            this.btn_Clear_WrEPC.TabIndex = 5;
            this.btn_Clear_WrEPC.Text = "Clear";
            this.btn_Clear_WrEPC.UseVisualStyleBackColor = true;
            this.btn_Clear_WrEPC.Click += new System.EventHandler(this.btn_Clear_WrEPC_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(115, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Serial Number";
            // 
            // txtbx_WrEPCSER
            // 
            this.txtbx_WrEPCSER.Location = new System.Drawing.Point(108, 43);
            this.txtbx_WrEPCSER.Name = "txtbx_WrEPCSER";
            this.txtbx_WrEPCSER.Size = new System.Drawing.Size(87, 20);
            this.txtbx_WrEPCSER.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Part Number";
            // 
            // txtbx_WrEPCPNO
            // 
            this.txtbx_WrEPCPNO.Location = new System.Drawing.Point(11, 43);
            this.txtbx_WrEPCPNO.Name = "txtbx_WrEPCPNO";
            this.txtbx_WrEPCPNO.Size = new System.Drawing.Size(87, 20);
            this.txtbx_WrEPCPNO.TabIndex = 1;
            // 
            // btn_Write_EPC
            // 
            this.btn_Write_EPC.Location = new System.Drawing.Point(56, 80);
            this.btn_Write_EPC.Name = "btn_Write_EPC";
            this.btn_Write_EPC.Size = new System.Drawing.Size(103, 23);
            this.btn_Write_EPC.TabIndex = 0;
            this.btn_Write_EPC.Text = "Write EPC";
            this.btn_Write_EPC.UseVisualStyleBackColor = true;
            this.btn_Write_EPC.Click += new System.EventHandler(this.btn_Write_EPC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 496);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btn_ConnectZD500R);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Connect_RZ400);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_Connect_RZ400;
        private System.Windows.Forms.Button btn_write;
        private System.Windows.Forms.ComboBox cmbx_WrBank;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbx_WrResult;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbx_WrData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbx_WrAddr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_read;
        private System.Windows.Forms.ComboBox cmbx_Bank;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbx_Addr;
        private System.Windows.Forms.Label Address;
        private System.Windows.Forms.TextBox txtbx_NumWrdsRead;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbx_RdData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Tries;
        private System.Windows.Forms.TextBox txtxbx_WrMyTries;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbx_WrChunkSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_ConnectZD500R;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtbx_RdEPCSER;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtbx_RdEPCPNO;
        private System.Windows.Forms.Button btn_Clear_RdEPC;
        private System.Windows.Forms.Button btn_Read_EPC;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn_Clear_WrEPC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbx_WrEPCSER;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtbx_WrEPCPNO;
        private System.Windows.Forms.Button btn_Write_EPC;
    }
}

