﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tego.Rfid.Gen2;
using ZPL_Driver;
using Tego.RFID.Device;
using Tego.RFID.ATA;

namespace ZPL_GUI
{
    public partial class Form1 : Form
    {
        ZPL rdr = new ZPL();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_read_Click(object sender, EventArgs e)
        {
            int duration = 300;
            IList<int> antennas = new List<int>() { 0 };
            Session session = Session.S0;
            Trigger trigger = new TimedRoundsTrigger(duration, 3);
            ExecutionSettings set_one_read = new ExecutionSettings(antennas, session, trigger);
            int start_A = Util.ParseHexOrDecValue(txtbx_Addr.Text);
            int read_length = (Util.ParseHexOrDecValue(txtbx_NumWrdsRead.Text));//convert Words to Bytes
            Tego.Rfid.Gen2.Bank read_bank = new Tego.Rfid.Gen2.Bank();
            if (cmbx_Bank.Text == Tego.Rfid.Gen2.Bank.Epc.ToString())
                read_bank = Tego.Rfid.Gen2.Bank.Epc;
            if (cmbx_Bank.Text == Tego.Rfid.Gen2.Bank.Reserved.ToString())
                read_bank = Tego.Rfid.Gen2.Bank.Reserved;
            if (cmbx_Bank.Text == Tego.Rfid.Gen2.Bank.Tid.ToString())
                read_bank = Tego.Rfid.Gen2.Bank.Tid;
            if (cmbx_Bank.Text == Tego.Rfid.Gen2.Bank.User.ToString())
                read_bank = Tego.Rfid.Gen2.Bank.User;
            int population = 1;
            var operations = new List<Operation>();
            operations.Add(new ReadOperation(read_bank, start_A, read_length, 0));
            ushort[] EPC_short = new ushort[0];
            Select selectThisEPC = new Select(Tego.Rfid.Gen2.Bank.Epc, 0, EPC_short, 0, SelectOperator.And);
            var responses = rdr.Execute(selectThisEPC, operations, set_one_read, population);
            if (responses != null)
            {
                string RD_String = "";
                var RD = rdr.Get_Read_Data(responses);
                for (int i = 0; i < RD.Length; i++)
                    RD_String += RD[i].ToString("X4");
                // txtbx_RdData.Text = responses[0].Operations[0].
                txtbx_RdData.Text = RD_String;
            }
        }

        private void btn_Connect_RZ400_Click(object sender, EventArgs e)
        {
            string Rz_400_ipAddress = "192.168.5.5";
            rdr.Connect(Rz_400_ipAddress);
        }

        private void btn_write_Click(object sender, EventArgs e)
        {

            int duration = 1000;
            IList<int> antennas = new List<int>() { 0 };
            Session session = Session.S0;
            Trigger trigger = new TimedRoundsTrigger(duration, 3);
            ExecutionSettings one_write = new ExecutionSettings(antennas, session, trigger);
            int start_A = Util.ParseHexOrDecValue(txtbx_WrAddr.Text);
            Tego.Utils.HexOrWords wr_data = new Tego.Utils.HexOrWords(txtbx_WrData.Text);
            //  Tego.Utils.HexOrWords wr_data = new Tego.Utils.HexOrWords("1234BEEF");
            Tego.Rfid.Gen2.Bank write_bank = new Tego.Rfid.Gen2.Bank();
            if (cmbx_WrBank.Text == Tego.Rfid.Gen2.Bank.Epc.ToString())
                write_bank = Tego.Rfid.Gen2.Bank.Epc;
            if (cmbx_WrBank.Text == Tego.Rfid.Gen2.Bank.Reserved.ToString())
                write_bank = Tego.Rfid.Gen2.Bank.Reserved;
            if (cmbx_WrBank.Text == Tego.Rfid.Gen2.Bank.Tid.ToString())
                write_bank = Tego.Rfid.Gen2.Bank.Tid;
            if (cmbx_WrBank.Text == Tego.Rfid.Gen2.Bank.User.ToString())
                write_bank = Tego.Rfid.Gen2.Bank.User;
            int population = 1;
            var operations = new List<Operation>();
            operations.Add(new WriteOperation(write_bank, start_A, wr_data, 0));
            ushort[] EPC_short = new ushort[0];
            Select selectThisEPC = new Select(Tego.Rfid.Gen2.Bank.Epc, 0, EPC_short, 0, SelectOperator.And);
            var responses = rdr.Execute(selectThisEPC, operations, one_write, population);

        }

        private void btn_ConnectZD500R_Click(object sender, EventArgs e)
        {

            string ZD500R_ipAddress = "192.168.5.9";
            rdr.Connect(ZD500R_ipAddress);
        }

        private void btn_Write_EPC_Click(object sender, EventArgs e)
        {
            string SEQ_value = txtbx_WrEPCSER.Text;
            string PNO_value = txtbx_WrEPCPNO.Text;
            string BR_XML = "<TEI><Name>" + "SEQ" + "</Name><Value>" + SEQ_value + "</Value></TEI><TEI><Name>PNO</Name><Value>" + PNO_value + "</Value></TEI><TEI><Name>" + "MFR" + "</Name><Value>" + "TEGOV" + "</Value></TEI><TEI><Name>UIC</Name><Value>" + "2" + "</Value></TEI>";
            EPCFilter Filter_Value = EPCFilter.Avionics;
            string ATAEPC = "";
            try
            {
                ATAEPC = XmlAITDevice.GenerateEPC(BR_XML, Filter_Value);
            }
            catch
            {
                MessageBox.Show("Could not generate an EPC");
                return;
            }


            // generate the PC word then move epc_data to start at address 2 in write data.
            string PC_Value = "";
            int EPC_Word_Length = ATAEPC.Length / 4;
            // Write protocol control (length in words of EPC)
            PC_Value = (EPC_Word_Length << 11).ToString("X4");
        //    Tego.Utils.HexOrWords PC_word = new Tego.Utils.HexOrWords(PC_Value);

            Tego.Utils.HexOrWords epc_data = new Tego.Utils.HexOrWords(PC_Value + ATAEPC);
            Tego.Rfid.Gen2.Bank write_bank = Tego.Rfid.Gen2.Bank.Epc;
            int duration = 1000;
            IList<int> antennas = new List<int>() { 0 };
            Session session = Session.S0;
            Trigger trigger = new TimedRoundsTrigger(duration, 3);
            ExecutionSettings one_write = new ExecutionSettings(antennas, session, trigger);
            int start_A = 1; //start at address 1 to write the PC word and the EPC data.
            int population = 1;
            var operations = new List<Operation>();
            operations.Add(new WriteOperation(write_bank, start_A, epc_data, 0));
      //      operations.Add(new PrintOperation("Tego", PNO_value, SEQ_value));
           // operations.Add(new WriteOperation(write_bank, 1, PC_word, 0));
            ushort[] EPC_short = new ushort[0];
            Select selectThisEPC = new Select(Tego.Rfid.Gen2.Bank.Epc, 0, EPC_short, 0, SelectOperator.And);
            var responses = rdr.Execute(selectThisEPC, operations, one_write, population);

        }

        private void btn_Read_EPC_Click(object sender, EventArgs e)
        {
            int my_tries = 3;
            int duration = 1000;
            IList<int> antennas = new List<int>() { 0 };
            Session session = Session.S0;
            Trigger trigger = new TimedRoundsTrigger(duration, 3);
            ExecutionSettings one_read = new ExecutionSettings(antennas, session, trigger);
            int start_A = 1; //start at address 1 to write the PC word and the EPC data.
            int population = 1;
            Tego.Rfid.Gen2.Bank read_bank = Tego.Rfid.Gen2.Bank.Epc;
            var operations = new List<Operation>();
            operations.Add(new ReadOperation(read_bank, start_A, 15, 0));
            // operations.Add(new WriteOperation(write_bank, 1, PC_word, 0));
            ushort[] EPC_short = new ushort[0];
            Select selectThisEPC = new Select(Tego.Rfid.Gen2.Bank.Epc, 0, EPC_short, 0, SelectOperator.And);
            string RD_String = "";
            string EPC_no_PC = "";
            for (int i = 0; i < my_tries; i++)
            {
                var responses = rdr.Execute(selectThisEPC, operations, one_read, population);
                if (responses != null)
                {
                    var RD = rdr.Get_Read_Data(responses);
                    for (int j = 0; j < RD.Length; j++)
                        RD_String += RD[j].ToString("X4");
                    i = my_tries + 1;
                }
            }
            if (RD_String == "")
            {
                Console.WriteLine("Could Not read EPC");
                return;
            }
            else
                EPC_no_PC = RD_String.Substring(4, RD_String.Length - 4);
        
            string Decoded_EPC = XmlAITDevice.DecodeEPC(EPC_no_PC);
            string[] words = Decoded_EPC.Split(' ');
            txtbx_RdEPCPNO.Text = words[1].ToString();
            txtbx_RdEPCSER.Text = words[2].ToString();
        }

        private void btn_Clear_RdEPC_Click(object sender, EventArgs e)
        {
            txtbx_RdEPCPNO.Text = "";
            txtbx_RdEPCSER.Text = "";
        }

        private void btn_Clear_WrEPC_Click(object sender, EventArgs e)
        {
            txtbx_WrEPCSER.Text = "";
            txtbx_WrEPCPNO.Text = "";
        }

        public static int ParseHexOrDecValue(string text)
        {
            int numBase = 10;
            if (text.StartsWith("0x") || text.StartsWith("0X"))
            {
                numBase = 16;
                text = text.Substring(2);
            }

            if (text == "")
                return -1;


            return Convert.ToInt32(text, numBase);
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
    }
}
